# changes by release

## 0.2.3
### other
* add '-H' option for host address (!4)
* rubocop - rescue exception var name

## 0.2.2
### features
* add an exclude option for _services_ check

## 0.2.1
### fixes
* fix wrong var name (#1)

## 0.2
### features
* new vm/lxc check modes

## 0.1.1
### fixes
* smart ok output changed to 'PASSED' (!1)
